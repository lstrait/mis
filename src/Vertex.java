
public class Vertex {
	int degree_;
	int[] neighbors_;
	
	public Vertex(int degree, int[] neighbors)
	{
		degree_ = degree;
		neighbors_ = neighbors;
	}
}
