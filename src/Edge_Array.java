
public class Edge_Array {
	
	public int rows_;
	public int cols_;
	public int non_zeros_;
	public Edge[] E_;
	
	public Edge_Array(Edge[] E, int rows, int cols, int non_zeros)
	{
		rows_ = rows;
		cols_ = cols;
		non_zeros_ = non_zeros;
		E_ = E;
	
	}
	
	public Edge_Array()
	{
		
	}
}
