
public class MISstep {
	/*
	 * For each vertex:
	 * Flags = 0 indicates undecided
	 * Flags = 1 indicates chosen
	 * Flags = 2 indicates a neighbor is chosen
	 */
	
	char flag;
	char[] flags_;
	Vertex[] G;
	
	public MISstep(char[] _F, Vertex[] _G)
	{
		flags_ = _F;
		G = _G;
	}
	
	public boolean reserve(int i)
	{
		int d = G[i].degree_;
		flag = 1;
		for(int j = 0; j < d; j++)
		{
			int ngh = G[i].neighbors_[j];
			if(ngh < i)
			{
				flag = 2;
				return true;
			}
			else if(flags_[ngh] == 0)
			{
				flag = 0;
			}
		}
		return true;
	}
	
	public boolean commit(int i)
	{
		return ((flags_[i] = flag) > 0);
	}
	
	public static char[] maximalIndependentSet(Graph GS)
	{
		int n = GS.n_;
		Vertex[] G = GS.V_;
		char[] flags = new char[n];
		for(int i = 0; i < flags.length; i++)
				flags[i] = ( (char) 0);
		MISstep mis = new MISstep(flags, G);
		// TODO: check correctness of speculative_for and underlying methods
		Reservation.speculative_for(mis,0,n,20);
		return flags;
	}
}
