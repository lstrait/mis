// Implements a graph using Adjacency Array Implementation
public class Graph {
	
	Vertex[] V_;
	int n_;
	int m_;
	int[] allocated_in_place_;
	
	public Graph(Vertex[] V, int n, int m)
	{
		V_ = V;
		n_ = n;
		m_ = m;
		allocated_in_place_ = null;
	}
	
	public Graph(Vertex[] V, int n, int m, int[] allocated_in_place)
	{
		V_ = V;
		n_ = n;
		m_ = m;
		allocated_in_place_ = allocated_in_place;
	}
	
	public Graph copy()
	{
		//TODO: fix 
		Vertex[] V_new = new Vertex[n_];
		int[] edges = new int[m_];
		int k = 0;
		for(int i = 0; i < n_; i++)
		{
			V_new[i] = V_[i];
			V_new[i].neighbors_ = new int[edges.length - k];
			int count  = 0;
			for(int j = k; i < edges.length; i++)
			{
				V_new[i].neighbors_[count] = edges[j];
				count++;
			}
			count = 0;
			
			for(int j = 0; j < V_[i].degree_; j++)
					edges[k++] = V_[i].neighbors_[j];
		}
		Graph temp = new Graph(V_new, n_, m_, edges);
		return temp;
		
	}
}
