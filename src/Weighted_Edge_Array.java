
public class Weighted_Edge_Array {

		int rows_;
		int cols_;
		Weighted_Edge[][] E_;
		
		public Weighted_Edge_Array(int rows, int cols)
		{
			rows_ = rows;
			cols_ = cols;
			E_ = new Weighted_Edge[rows_][cols_];
			
		}
		
}
