import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import java.util.*;
public class GraphIO {
	public static String AdjGraphHeader = "AdjacencyGraph";
	public static Graph readGraphFromFile(File f) throws IOException
	{
		String s = FileUtils.readFileToString(f);
		Words w = new Words(s, (long)s.length());
		System.out.println(w.strings[0]);
		if(!w.strings[0].equals(AdjGraphHeader))
		{
			System.out.println("Bad input file1");
			System.exit(0);
		}
		
		int len = (int)(w.m) -1 ; 
		int[] In = new int[len];
		//parallel for loop in Cilk code
		for(int i = 0; i < len; i++)
		{
			In[i] = Integer.parseInt(w.strings[i+1]);
		}
		
		int j = In[0];
		int k = In[1];
		if(len != j + k +2)
		{
			System.out.println("Bad input file2");
			System.exit(0);
		}
		Vertex[] v = new Vertex[j];
		//int offset = In[2];
		//int edge = In[2+j];
		
		int[] offsets =  new int[In.length - 2];
		for(int z = 0; z < offsets.length; z++)
				offsets[z] = In[z+2];
		
		int[] edges =  new int[In.length - 2- j];
		for(int z2 = 0; z2 < edges.length; z2++ )
				edges[z2] = In[z2+2+j];
		
		//parallel for loop in Cilk code
		for(int i = 0; i < j; i ++)
		{
			int o = offsets[i];
			int l = ((i ==  j - 1) ? k : offsets[i+1] - offsets[i]);
			//v[i].degree_ = l;
			System.out.println(edges.length);
			System.out.println(o);
			int[] temp =  new int[edges.length - o];
			for(int z3 = 0; z3 < temp.length; z3++ )
				temp[z3] = edges[z3+o];
			//v[i].neighbors_ = temp;
			v[i] = new Vertex(l, temp);
		}
		Graph g =  new Graph(v,j,k,In);
		return g;
	}
	
	
	// create a random graph with edges of dimension dim in File f
	public static void writeRandomGraphToFile(File f, int dim)
	{
			Edge_Array EA = edgeRandomWithDimension(3, 4, 2);
			Graph g = GraphFromEdges(EA);
			//TODO: finish the method
			
	}
	
	public static Edge_Array edgeRandomWithDimension(int dim, int degree, int numRows)
	{
		int nonzeros = numRows * degree;
		Edge[] E = new Edge[nonzeros];
		//parallel for loop in Cilk code
		for(int k = 0; k < nonzeros; k++)
		{
			int i = k / degree;
			int j;
			if(dim == 0)
			{
				int h = k;
				do
				{
					j = ((h = Utils.hash(h)) % numRows);
					
				}while(j == i);
			}
			else
			{
				int pow = dim +2;
				int h = k;
				do
				{
					while (((h = Utils.hash(h)) % 1000003) < 500001)
						pow += dim;
					j = (i + ((h = Utils.hash(h)) % ((1) << pow))) % numRows;
				}while(j == i);
			}
			E[k].u_ = i;
			E[k].v_ = j;
		}
		Edge_Array EA = new Edge_Array(E, numRows, numRows, nonzeros);
		return EA;
	}
	
	public static Graph GraphFromEdges(Edge_Array EA)
      {

		Edge_Array A = new Edge_Array();
		Edge[] E =  new Edge[EA.non_zeros_];
		//parallel for loop in the CILK code
		for(int i = 0; i < EA.non_zeros_; i++)
			E[i] = EA.E_[i];
		A  = new Edge_Array(E, EA.rows_, EA.cols_, EA.non_zeros_);
		
		
		int m = A.non_zeros_;
		int n = Math.max(A.cols_, A.rows_);
		int[] offsets = new int[n*2];
		
		Arrays.sort(A.E_);
		
		int curr = A.E_[0].u_;
		int offset = 1;
		offsets[0] = 0;
		for(int x = 1; x < A.E_.length; x++)
		{
			if(A.E_[x].u_ == curr)
			{
				//curr = A.E_[x].u_;
				continue;
			}
			else
			{
				offsets[offset] = x;
				offset++;
				curr = A.E_[x].u_;
			}
		}
		int[] X = new int[m];
		Vertex[] v =  new Vertex[n];
		// parallel for loop in the CILK code
		for(int j = 0; j < n; j++)
		{
			 int o = offsets[j];
			 int l = ((j == n - 1) ? m : offsets[j+1]) - offsets[j];
			 v[j].degree_ = l;
			 v[j].neighbors_ = new int[l];
			 for(int k = 0; k < l; k++)
			 {
				 v[j].neighbors_[k] = A.E_[o+k].v_;
				 X[o+k] = A.E_[o+k].v_;
			 }
		}
		Graph g =  new Graph(v, n, m, X);
		return g;
		
	  }

}
