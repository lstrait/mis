
public class Words {
	long n;
	long m;
	String string;
	String[] strings;
	
	public  Words(long n_, String chars_, String[] strings_)
	{
		n = n_;
		m = strings_.length;
		string = chars_;
		strings = strings_;
	}
	
	// constructor to create Words from a string
	public Words(String str, long n_)
	{
		strings = str.split("\r?\n|\r");
		string = str;
		n = n_;
		m = strings.length;
		
	}
}
