
public class Seq {
	int[] A;
	long n;
	public Seq()
	{
		A = null;
		n = 0;
	}
	public Seq(int[] A_, long n_)
	{
		A = A_;
		n = n_;
	}
	
	public long pack(int[] In, int[] Out, boolean[] Fl, int n)
	{
		getA temp = new getA(In);
		return pack(Out, Fl, 0, n, temp).n;
	}
	
	public int nblocks(int n, int bsize)
	{
		return (1 + (n-1)/bsize);
	}
	
	public Seq pack(int[] Out, boolean[] Fl, int s, int e, getA f)
	{
		int _F_BSIZE = 2048;
		int l = nblocks(e-s, _F_BSIZE);
		if(l <= 1)
				return packSerial(Out, Fl, s, e, f);
		int[] Sums =  new int[l];
		// Do the work from blocked for loop
		int ss = s;
		int ee = e;
		int n = ee - ss;
		int l2 = nblocks(n, _F_BSIZE);
		// parallel for loop in code
		for(int i = 0; i < l2; i++)
		{
			//TODO
			boolean[] temp = new boolean[Fl.length - s];
			int count = 0;
			for(int j = s; j < Fl.length; j++)
			{
				temp[count] = Fl[j];
				count++;
			}
			Sums[i] = sumFlagsSerial(temp, e-s);
		}
		// end work of blocked for loop
		int m = plusScan(Sums, Sums, l);
		if(Out == null)
			Out = new int[m];
		// Do the work from second blocked for loop
		ss = s;
		ee = e;
		n = ee - ss;
		l2 = nblocks(n, _F_BSIZE);
		for(int i = 0; i < l2; i++)
		{
			//TODO
			//TODO
		}
		// end work of blocked for loop
		Seq temp = new Seq(Out, m);
		return temp;
	}
	
	public Seq packSerial(int[] Out, boolean[] Fl, int s, int e, getA f)
	{ 
		
		if(Out == null)
		{
			boolean[] temp = new boolean[Fl.length - s];
			int count = 0;
			for(int i = s; i < Fl.length; i++)
			{
				temp[count] = Fl[i];
				count++;
			}
			int m = sumFlagsSerial(temp, e-s);
			Out = new int[m];
		}
		int k = 0;
		for(int i = 0; i < e; i++)
		{
			if(Fl[i])
			{
				Out[k++] = f.get(i);
			}
		}
		Seq ret = new Seq(Out,k);
		return ret;
	}
	
	public int sumFlagsSerial(boolean[] Fl, int n)
	{
		int r = 0;
		for(int j = 0; j < n; j++)
		{
			if(Fl[j] == true)
				r++;
		}
		return r;
	}
	
	public int plusScan(int[] In, int[] Out, int n)
	{
		addF f = new addF();
		getA g = new getA(In);
		return scan(Out, 0, n, f, g, 0, false, false);
	}
	
	public int scan(int[] Out, int s, int e, addF f, getA g, int zero, boolean inclusive, boolean back)
	{
		int n = e -s;
		int F_BSIZE = 2400;
		int l = nblocks(n, F_BSIZE);
		if(l <= 2)
			return scanSerial(Out, s, e, f, g, zero, inclusive, back);
		int[] sums = new int[l];
	    for(int i = 0; i < F_BSIZE; i++)
					sums[i] = reduceSerial(s, e, f, g);
	    getA temp = new getA(sums);
		int total = scan(sums, 0, l, f, temp, zero, false, back);
		for(int j = 0; j < F_BSIZE; j++)
			scanSerial(Out, s, e, f, g, sums[j], inclusive, back);
		return total;
	}
	
	int scanSerial(int[] Out, int s, int e, addF f, getA g, int zero, boolean inclusive, boolean back)
	{
		int r = zero;
		if(inclusive)
		{
			if(back)
			{
				for(int i = e - 1; i >= s; i--)
					Out[i] = r = f.get(r, g.get(i));
			}
			else
			{
				for(int i = s; i < e; i++)
					Out[i] = r = f.get(r, g.get(i));
			}
		}
		else
		{
			if(back)
			{
				for(int i = e -1; i >= s; i--)
				{
					int t = g.get(i);
					Out[i] = r;
					r = f.get(r,t);
				}
			}
			else
			{
				for(int i = s; i < e; i++)
				{
					int t = g.get(i);
					Out[i] = r;
					r = f.get(r, t);
				}
			}
		}
		return r;
	}
	
	int reduceSerial(int s, int e, addF f, getA g)
	{
		int r = g.get(s);
		for(int j = s +1; j < e; j++)
			r = f.get(r, g.get(j));
		return r;
	}
	
}
