
public class Reservation {

	int r;
	
	public Reservation()
	{
		r = Integer.MAX_VALUE;
	}
	
	public void reserve(int i)
	{
		//TODO: Implement
	}
	
	public boolean reserved()
	{
		return ( r < Integer.MAX_VALUE);
	}
	
	public void reset()
	{
		r = Integer.MAX_VALUE;
	}
	
	public boolean check(int i)
	{
		return (r == i);
	}
	
	public boolean check_reset(int i)
	{
		if(r == i)
		{
			r = Integer.MAX_VALUE;
			return true;
		}
		else
			return false;
	}
	
	public static void reserve_loc(int x, int i)
	{
		//TODO: Implement
	}
	
	public static int speculative_for(MISstep step, int s, int e, int granularity)
	{
			boolean hasState = true;
			int maxTries = -1;
			if(maxTries < 0)
				maxTries = 100 + 200 * granularity;
			int maxRoundSize = (e-s)/ granularity + 1;
			int[] I = new int[maxRoundSize];
			int[] Ihold =  new int[maxRoundSize];
			boolean[] keep = new boolean[maxRoundSize];
			MISstep[] state = null;
			if(hasState)
			{
				state = new MISstep[maxRoundSize];
				for(int i = 0; i < maxRoundSize; i++)
					state[i] = step;
			}
			
			int round = 0;
			int numberDone = s;
			int numberKeep = 0;
			int totalProcessed = 0;
			
			while(numberDone < e)
			{
				if(round++ > maxTries)
				{
					throw new IllegalArgumentException("speculativeLoop: too many iterations, increase maxTries parameter");
				}
				int size = Math.min(maxRoundSize, e- numberDone);
				totalProcessed += size;
				
				if(hasState)
				{
					// parallel loop in other code
					for(int i = 0; i < size; i++)
					{
						if(i >= numberKeep)
							I[i] = numberDone + i;
						keep[i] = state[i].reserve(I[i]);
					}
				}
				else
				{
					// parallel loop in other code
					for(int i = 0; i < size; i++)
					{
						if(i >= numberKeep)
							I[i] = numberDone + i;
						keep[i] = step.reserve(I[i]);
					}
				}
				
				if(hasState)
				{
					// parallel loop in other code
					for(int i = 0; i < size; i++)
					{
						if(keep[i])
							keep[i] = !state[i].commit(I[i]);
				
					}
				}
				else
				{
					for(int i = 0; i < size; i++)
					{
						if(keep[i])
							keep[i] = !step.commit(I[i]);
					}
				}
				
				Seq sq = new Seq();
			    sq.pack(I, Ihold, keep, size);
				int[] temp =  null;
				temp = I;
				I = Ihold;
				Ihold = temp;
				numberDone += size - numberKeep;
			}
			return totalProcessed;
	}
	
	
}
