
public class Edge implements Comparable<Edge> {
	
	public int u_;
	public int v_;
	
	public Edge(int u, int v)
	{
		u_ = u;
		v_ = v;
	}
	
	
	public int compareTo(Edge compareEdge)
	{
		int compareQuantity = compareEdge.u_;
		return this.u_ - compareQuantity;
		
	}
	
}
